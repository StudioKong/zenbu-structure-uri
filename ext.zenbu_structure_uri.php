<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Zenbu Structure URI support extension
 * =========================================
 * Enables display of Structure URI in Zenbu
 * @version 	1.0.0 
 * @author 		Koen Veestraeten (@StudioKong)
 * Based on the examples Zenbu MX Cloner and Zenbu Tag Formatting provided by Nicolas Bottari
 * Zenbu MX Cloner: https://github.com/nicolasbottari/zenbu_mx_cloner.zenbu_addon.ee2_addon
 * Zenbu Tag Formatting: https://github.com/nicolasbottari/zenbu_tag_formatting.zenbu_addon.ee2_addon
 * -----------------------------------------
 * 
 * *** IMPORTANT NOTES ***
 * I (Koen Veestraeten) am not responsible for any
 * damage, data loss, etc caused directly or indirectly by the use of this extension. 
 *
 * REQUIRES 
 * Zenbu module:
 * @link	http://nicolasbottari.com/expressionengine_cms/zenbu/
 * Hooks to extend Zenbu:
 * @see		http://nicolasbottari.com/expressionengine_cms/dev/zenbu
 *
 * This assumes you are using Structure or pages module.
 */
class Zenbu_structure_uri_ext {
	
	var $name				= 'Zenbu Structure URI support extension';
	var $addon_short_name 	= 'zenbu_structure_uri';
	var $version 			= '1.0.0'; 
	var $description		= 'Enables display of the Structure URI in Zenbu';
	var $settings_exist		= 'n';
	var $docs_url			= '';
	var $settings        	= array();

	/**
	* Constructor
	*
	* @param 	mixed	Settings array or empty string if none exist.
	*/
	function __construct($settings='')
	{
		$this->EE =& get_instance();
		$this->settings = $settings;
		$this->EE->lang->loadfile('zenbu_structure_uri');
	}
	
	/**
	* ===============================
	* function hook_zenbu_add_column
	* ===============================
	* Adds a row in Zenbu's Display settings section
	* @return array 	$output 	An array of data used by Zenbu
	* The $output array must have the following keys:
	* column: Computer-readable used as identifier for settings. Keep it unique!
	* label: Human-readable label used in the Display settings row.
	*/
	function hook_zenbu_add_column()
	{
		// Get whatever was passed through this hook from previous add-ons
		$field = $this->EE->extensions->last_call;

		// Add all fields

			$field[] = array(
			'column'	=> 'show_structure_uri',							// Computer/Cylon-readable
			'label'		=> $this->EE->lang->line('structure_uri')			// Human-readable
			);
			
			
		return $field;
	}

	/**
	* ======================================
	* function hook_zenbu_entry_cell_data
	* ======================================
	* Adds data to a Zenbu entry row
	* @param int 	$entry_id 		The current Entry ID
	* @param array $entry_array 	An array of all entries found by Zenbu
	* @param int 	$channel_id 	The current channel ID for the entry
	* 
	* @return array 	$output 	An array of data used by Zenbu. 
	* The key must match the computer-readable identifier, minus the 'show_' part.
	*/
	 
	function hook_zenbu_entry_cell_data($entry_id, $entry_array, $channel_id)
	{
			$site_id = $this->EE->config->item('site_id'); // Get site id (MSM safety)
			$site_pages = $this->EE->config->item('site_pages'); // Get pages array
			if ( isset($site_pages[$site_id]['uris'][$entry_id]) )
			{
				$page_url = $site_pages[$site_id]['uris'][$entry_id];
				$output['structure_uri'] = '<a href="' .$page_url. '">' .$page_url. '</a>';
				return $output;
			}
	}

	
	/**
	* Settings Form
	*
	* @param	Array	Settings
	* @return 	void
	*/
	function settings_form()
	{	
		return "";			
	}
	
	/**
	* Save Settings
	*
	* This function provides a little extra processing and validation 
	* than the generic settings form.
	*
	* @return void
	*/
	function save_settings()
	{
		if (empty($_POST))
		{
			show_error($this->EE->lang->line('unauthorized_access'));
		}
		
		unset($_POST['submit']);
		
		$settings = $_POST;
		
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->update('extensions', array('settings' => serialize($settings)));
		
		$this->EE->session->set_flashdata(
			'message_success',
		 	$this->EE->lang->line('preferences_updated')
		);
	}



	function activate_extension() {
	
			$data[] = array(
			    'class'      => __CLASS__,
			    'hook'      => "zenbu_add_column",
				'method'    => "hook_zenbu_add_column",
			    'settings'    => serialize(array()),
			    'priority'    => 110,
			    'version'    => $this->version,
			    'enabled'    => "y"
			  );

			$data[] = array(
			    'class'      => __CLASS__,
			    'hook'      => "zenbu_entry_cell_data",
			    'method'    => "hook_zenbu_entry_cell_data",
			    'settings'    => serialize(array()),
			    'priority'    => 110,
			    'version'    => $this->version,
			    'enabled'    => "y"
			  );

	    	
		      	
	      // insert in database
	      foreach($data as $key => $data) {
	      $this->EE->db->insert('exp_extensions', $data);
	      }
	  }
	
	
	function disable_extension() {

	  $this->EE->db->where('class', __CLASS__);
	  $this->EE->db->delete('exp_extensions');

	} 
	  
	  /**
	 * Update Extension
	 *
	 * This function performs any necessary db updates when the extension
	 * page is visited
	 *
	 * @return 	mixed	void on update / false if none
	 */
	function update_extension($current = '')
	{
		if ($current == '' OR $current == $this->version)
		{
			return FALSE;
		}
		
		if ($current < $this->version)
		{
			// Update to version 1.0
		}
		
		$this->EE->db->where('class', __CLASS__);
		$this->EE->db->update(
					'extensions', 
					array('version' => $this->version)
		);
	}
  

}
// END CLASS